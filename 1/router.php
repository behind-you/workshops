<?php
require_once 'Controllers/MainController.php';
$mainController = new Controllers\MainController();

if ($route == '/') {
    return $mainController->index();
}

if (isset($route[1]) && method_exists($mainController, $route[1])) {
    return $mainController->{$route[1]}();
}

return $mainController->notFound();
