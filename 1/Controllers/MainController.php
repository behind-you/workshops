<?php

namespace Controllers;

class MainController
{
    public function index()
    {
        echo file_get_contents(__DIR__ . '/../Views/index.html');
        exit;
    }

    public function overview()
    {
        echo file_get_contents(__DIR__ . '/../Views/overview.html');
        exit;
    }

    public function add()
    {
        echo file_get_contents(__DIR__ . '/../Views/add.html');
        exit;
    }

    public function notFound()
    {
        echo file_get_contents(__DIR__ . '/../Views/404.html');
        exit;
    }
}
