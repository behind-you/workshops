<?php

require_once __DIR__ . '/vendor/autoload.php';

use Behindyou\Router;

$router = new Router();

echo $router->resolve();
