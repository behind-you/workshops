<?php

namespace Behindyou;

use Behindyou\Controllers\MainController;

class Router
{
    /** @var string */
    private $route;

    /** @var MainController */
    private $mainController;

    public function __construct()
    {
        $this->route = $this->getPath();
        $this->mainController = new MainController();
    }

    /**
     * @return array
     */
    private function getPath(): array
    {
        return isset($_SERVER['PATH_INFO']) ? explode('/', ltrim($_SERVER['PATH_INFO'])) : ['/'];
    }

    /**
     * @return string
     */
    public function resolve(): string
    {
        if ($this->route == ['/']) {
            return $this->mainController->index();
        }
        
        if (isset($this->route[1]) && method_exists($this->mainController, $this->route[1])) {
            return $this->mainController->{$this->route[1]}();
        }
        
        return $this->mainController->notFound();
    }
}
