<?php

namespace Src\tests;

use App\Router;
use PHPUnit\Framework\TestCase;

final class RouerTest extends TestCase
{
    private function setRequestedRoute(string $route): void
    {
        $_SERVER['PATH_INFO'] = $route;
    }

    public function testHomeRouteIsReturnedCorrectly(): void
    {
        $router = new Router();

        $this->assertRegExp('/Mitarbeiterverwaltung/', $router->resolve());
    }

    public function testAddRouteIsReturnedCorrectly(): void
    {
        $this->setRequestedRoute('/add');

        $router = new Router();

        $this->assertRegExp('/Mitarbeiter hinzufügen/', $router->resolve());
    }

    public function testOverviewRouteIsReturnedCorrectly(): void
    {
        $this->setRequestedRoute('/overview');

        $router = new Router();

        $this->assertRegExp('/Mitarbeiterübersicht/', $router->resolve());
    }

    public function testUnknownRouteResolvesTo404(): void
    {
        $this->setRequestedRoute('/unknown-route');

        $router = new Router();

        $this->assertRegExp('/404/', $router->resolve());
    }
}
