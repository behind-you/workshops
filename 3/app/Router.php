<?php

namespace App;

use App\Controllers\MainController;

class Router
{
    /** @var string */
    private $route;

    /** @var MainController */
    private $mainController;

    public function __construct()
    {
        $this->route = $this->extractPathInfo();
        $this->mainController = new MainController();
    }

    /**
     * @return array
     */
    private function extractPathInfo(): array
    {
        return isset($_SERVER['PATH_INFO']) ? explode('/', ltrim($_SERVER['PATH_INFO'])) : ['/'];
    }

    /**
     * Returns response corresponding to route.
     *
     * @return string
     */
    public function resolve(): string
    {
        if ($this->route == ['/']) {
            return $this->mainController->index();
        }

        if (isset($this->route[1]) && method_exists($this->mainController, $this->route[1])) {
            return $this->mainController->{$this->route[1]}();
        }

        return $this->mainController->notFound();
    }
}
