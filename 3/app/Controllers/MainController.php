<?php

namespace App\Controllers;

class MainController
{
    public function index()
    {
        return file_get_contents(__DIR__ . '/../Views/index.html');
    }

    public function overview()
    {
        return file_get_contents(__DIR__ . '/../Views/overview.html');
    }

    public function add()
    {
        return file_get_contents(__DIR__ . '/../Views/add.html');
    }

    public function notFound()
    {
        return file_get_contents(__DIR__ . '/../Views/404.html');
    }
}
